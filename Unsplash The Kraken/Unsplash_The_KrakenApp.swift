//
//  Unsplash_The_KrakenApp.swift
//  Unsplash The Kraken
//
//  Created by Ihwan ID on 13/12/20.
//

import SwiftUI

@main
struct Unsplash_The_KrakenApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
