//
//  SearchResponse.swift
//  Unsplash The Kraken
//
//  Created by Ihwan ID on 13/12/20.
//

import Foundation

struct PhotoResponse: Decodable {
  let results: [Photo]
}

struct Photo: Decodable {

    let urls: Urls

}

struct Urls: Decodable {

    let raw: String
    let full: String
    let regular: String
    let small: String
    let thumb: String

}
