//
//  ContentView.swift
//  Unsplash The Kraken
//
//  Created by Ihwan ID on 13/12/20.
//

import SwiftUI

struct ContentView: View {
    @Namespace var namespace
    @State var searchText = "office"
    @State var images:[String] = []
    var body: some View {
        NavigationView{
            VStack{
                ScrollView(.vertical, showsIndicators: false){
                    SearchBar(text: $searchText, placeholder: "Search")
                        .onChange(of: searchText, perform: { value in
                            getData()
                        })
                    LazyVGrid(
                        columns:[ GridItem(.flexible()),GridItem(.flexible())],
                        spacing: 16) {
                        ForEach(images, id: \.self) { image in

                            RemoteImage(url: image)

                                .cornerRadius(15)

                        }
                    }
                }.animation(.spring(response: 0.4, dampingFraction: 0.8))
                .padding(.horizontal, 20)
            }
            .onAppear{
                getData()
            }.navigationTitle("Unsplash The Kraken")
        }
    }

    func getData(){
        var request = URLRequest(url: URL(string: "https://api.unsplash.com/search/photos?page=1&query=\(searchText)")!)
        request.httpMethod = "GET"
        request.setValue("Client-ID d8a272c480b258b875d82f4062d6c52e4ae7f4b4656add778d71e9b638b2f8be", forHTTPHeaderField: "Authorization")
        URLSession.shared.dataTask(with: request){ data, response, error in
            if let data = data {
                if let decodedResponse = try? JSONDecoder().decode(PhotoResponse.self, from: data){
                    DispatchQueue.main.async{
                        self.images = decodedResponse.results.map({ (image) -> String in
                            return image.urls.small
                        })
                    }
                    return
                }

            }
        }.resume()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

